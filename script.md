# Tutorial 1 - Making a house sales recommendation engine

1. Get the data
2. Preprocess the data
3. Upload the data
4. Create a project
5. Create a model
6. Train a model
7. Make predictions
9. Done
