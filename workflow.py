import json
import sys
import statistics as stat
import requests

username = "johnnybanana@fruits.com"
password = "ridiculouslysecret"
project = "Tutorial1"
team_id = "d2-team-mike"

%run "credentials.py"

base_url = "https://app.alviss.io"
core_path = base_url + "/api/v1/core"
comnav_path = base_url + "/api/v1/commercial-navigator"


def err_exit(msg):
    print(msg, file=sys.stderr)
    exit(1)


# get token
url = base_url + "/access_token"

r = requests.post(url, data={"username": username, "password": password})

if r.ok:
    token = r.json().get("access_token")
else:
    err_exit("Failed to get token")

# List ComNav projects
headers = {"X-Team-Id": team_id, "Authorization": "Bearer " + token}
r = requests.get(comnav_path + "/project/list", headers=headers)
r.json()

# List Core projects
headers = {"X-Team-Id": team_id, "Authorization": "Bearer " + token}
r = requests.get(core_path + "/project/list", headers=headers)
r.json()

# Create a project
headers = {"X-Team-Id": team_id, "Authorization": "Bearer " + token}
d = {
    "name": project,
    "description": "The project we are using for Tutorial1",
    "color": "#0000FF",
}
r = requests.post(core_path + "/project", headers=headers, json=d)
r.json()

# Upload data - Create a folder
headers = {"X-Team-Id": team_id, "Authorization": "Bearer " + token}
r = requests.post(
    core_path + "/storage/folder", headers=headers, params={"path": "tutorial1"}
)
r.json()

# Upload data - Upload a file
headers = {
    "X-Team-Id": team_id,
    "Authorization": "Bearer " + token,
}
params = {
    "path": "tutorial1",
    "overwrite": "false",
    "unarchiving": "false",
}
files = {
    "file": (
        "sales_price_alviss.csv",
        open("data/sales_price_alviss.csv", "rb"),
        "text/csv",
    )
}
r = requests.post(
    core_path + "/storage/file", headers=headers, params=params, files=files
)
r.content

# Upload data - Check that the file is there
headers = {"X-Team-Id": team_id, "Authorization": "Bearer " + token}
params = {"path": "tutorial1"}
r = requests.get(core_path + "/storage/list", headers=headers, params=params)
r.json()

# Train model
headers = {"X-Team-Id": team_id, "Authorization": "Bearer " + token}
params = {
    # "name": "Jojo", # This has to be unique or left out!!!
    # "description": "Heyhey",
    "capability": "time_series",
    "version": "v4_0_2",
    "function_name": "train",
}
d = {
    "train_params": {
        "architectures": [
            "LinearRegression",
            # "LinearRegressionWithSeason",
            # "LinearRegressionWithSingleSeason",
            # "MLP",
            "MLPNarrow",
            # "MLPWithSeason",
            # "MLPWithSingleSeason",
        ],
        "epochs": 2000,
        "optimizer": "AdamW(lr=1e-3)",
        "samples": 2,
        "samples_validation": 2,
        "train_csv": "tutorial1/sales_price_alviss.csv",
    },
    "data_params": {
        "season": True,
        "drop_na": False,
        "inputs": ["floor", "rooms", "sqm", "year"],
        "targets": ["price"],
        "agglev": "day",
        "types": {"price": "Real"},
    },
}
r = requests.post(core_path + "/job/submit", headers=headers, params=params, json=d)
trnjobid = r.headers['X-Job-Id']
r.json()

# Predict from trained model 
# Make sure the last job run was the train job
num_samples = 200
headers = {"X-Team-Id": team_id, "Authorization": "Bearer " + token}
params = {
    # "name": "Jojo", # This has to be unique or left out!!!
    # "description": "Heyhey",
    "capability": "time_series",
    "version": "v4_0_2",
    "function_name": "predict",
    #"parent_ids": "6zufRHQdxfmCSJL", # This must be the job you previously trained!
    "parent_ids": trnjobid, # This must be the job you previously trained!
}
d = {
    "predict_params": {
        "samples": num_samples,
        "input": {
            "date": [
                "2022-05-01",
            ],
            "floor": [
                6,
            ],
            "rooms": [
                2,
            ],
            "year": [
                2020,
            ],
            "sqm": [
                55,
            ],
        },
    },
}
r = requests.post(core_path + "/job/submit", headers=headers, params=params, json=d)
predictions = [r.json()["data"][i]["value"] for i in range(num_samples)]
mu = stat.mean(predictions)
sigma = stat.stdev(predictions)
print(f"The value of this apartment should be sold for {mu:,.0f} ± {sigma:,.0f} kr.")

